# def maxsubarray(nums):
#     list_of_nums = []

#     i = 0
#     while i < len(nums):
#         j = i + 1
#         while j <= len(nums):
#             subarray = nums[i:j]
#             sum_subarray = sum(subarray)
#             list_of_nums.append(sum_subarray)
#             j += 1
#         i += 1
#     return max(list_of_nums)

# print(maxsubarray([-2,1,-3,4,-1,2,1,-5,4]))
# print(maxsubarray([1]))
# print(maxsubarray([5,4,-1,7,8])) 

def maxsubarray(nums):
    curr = 0
    maxsub = nums[0]

    for num in nums:
        if curr < 0:
            curr = 0
        curr += num
        maxsub = max(maxsub, curr)
    return maxsub

print(maxsubarray([-2,1,-3,4,-1,2,1,-5,4]))
print(maxsubarray([1]))
print(maxsubarray([5,4,-1,7,8])) 