# https://leetcode.com/problems/two-sum/

def twosum(nums = [2,7,11,15], target=9):
    new_dict = {} # hashmap. value : index

    for i, n in enumerate(nums): # i = index and n = number 
        diff = target - n # calculate difference where n is incremented through values in nums
        if diff in new_dict:
            return [new_dict[diff], i] 
        new_dict[n] = i # if solution not found the value and index is added to dict 

print(twosum(nums = [2,7,11,15], target=9))