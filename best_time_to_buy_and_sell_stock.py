# https://leetcode.com/problems/best-time-to-buy-and-sell-stock/

# Approach: initialise 2 pointers, iterate through list
# have condition to check left is < right because you can only 
# sell after you buy else left if pointer is = right.

def maxProfit(prices):
    l, r = 0, 1 # left and right ptr
    maxP = 0 # maxProfit

    while r < len(prices):
        if prices[l] < prices[r]:
            profit = prices[r] - prices[l]
            maxP = max(maxP, profit)
        else:
            l = r
        r += 1
    return maxP

print(maxProfit(prices = [7,1,5,3,6,4]))