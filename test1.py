def maxsubarray(nums):
    curr = 0
    maxsub = nums[0]

    for num in nums:
        if curr < 0:
            curr = 0
        curr += num
        maxsub = max(maxsub, curr)
    return maxsub

print(maxsubarray([-2,1,-3,4,-1,2,1,-5,4]))
print(maxsubarray([1]))
print(maxsubarray([5,4,-1,7,8])) 
