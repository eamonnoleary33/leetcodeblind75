# def containsDuplicate(nums): O(n^2) as count has O(n)
#     for num in nums:
#         if nums.count(num) != 1:
#             return True
#     return False

def containsDuplicate(nums): # O(n)
    hashset = set()

    for num in nums:
        if num in hashset:
            return True
        hashset.add(num)
    return False

print(containsDuplicate([2,14,18,22,22]))